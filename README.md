# Test task

### Project setup

To run this project, first of all you should setup firebase, i suggest use flutterfire_cli with firebase_cli.

[Firebase setup tutorial](https://firebase.google.com/docs/flutter/setup?platform=android)

### Project technologies

State managment - [Riverpod](https://riverpod.dev)
Localization - [Intl](https://pub.dev/packages/intl)
Auth service - [Firebase](https://pub.dev/packages/firebase_auth)
Go router - [GoRouter](https://pub.dev/packages/go_router)
Freezed - [Freezed](https://pub.dev/packages/freezed)

Also static code analyzer was enabled.