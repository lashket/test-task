import 'package:flutter_test/flutter_test.dart';
import 'package:scale_3c_test/common/utils/extensions.dart';

import '../../helpers/constants.dart';

void main() {

  group('Email validation tests', () {
    test('Correct email test', () {
      expect(correctEmail.emailValid, true);
    });
    test('Incorrect email test', () {
      expect(incorrectEmail.emailValid, false);
    });
  });

  group('Password validation tests', () {
    test('Correct password test', () {
      expect(correctPassword.passwordValid, true);
    });
    test('Incorrect password test', () {
      expect(incorrectPassword.passwordValid, false);
    });
  });

}