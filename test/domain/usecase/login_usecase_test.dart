import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:scale_3c_test/common/exception.dart';
import 'package:scale_3c_test/domain/constants.dart';
import 'package:scale_3c_test/domain/repository/auth_repository.dart';
import 'package:scale_3c_test/domain/usecase/auth/login_usecase.dart';

import '../../helpers/mocks.mocks.dart';

void main() {
  late AuthRepository authRepository;

  late String tEmail;
  late String tPassword;

  late LoginUsecase loginUsecase;

  setUp(() {
    authRepository = MockAuthRepository();
    tEmail = 'lashket@gmail.com';
    tPassword = '12345678';
    loginUsecase = LoginUsecase(authRepository: authRepository);
  });

  group('Login tests', () {
    test('Login success', () async {
      when(authRepository.signIn(email: tEmail, password: tPassword)).thenAnswer((Invocation value) async => Future<void>);
      final Either<AppException, Unit> result = await loginUsecase(email: tEmail, password: tPassword);
      expect(result, isNot(Exception()));
    });
    test('Login not success', () async {
      when(authRepository.signIn(email: tEmail, password: tPassword)).thenThrow(FirebaseAuthException(code: firebaseNotFoundCode));
      final Either<AppException, Unit> result = await loginUsecase(email: tEmail, password: tPassword);
      expect(result, equals(left(AppException.invalidCredentials())));
    });
  });
}
