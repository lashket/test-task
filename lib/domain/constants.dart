const String firebaseWrongPasswordCode = 'wrong-password';
const String firebaseNotFoundCode = 'user-not-found';
const String firebaseEmailInUseCode = 'email-already-in-use';
