import '../../entity/user_profile.dart';
import '../../repository/auth_repository.dart';

class CurrentUserStreamUseCase {

  CurrentUserStreamUseCase({required this.authRepository});

  final AuthRepository authRepository;

  Stream<UserProfile?> call() => authRepository.userProfileStream();


}