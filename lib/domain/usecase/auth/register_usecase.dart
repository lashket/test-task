import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../../common/exception.dart';
import '../../constants.dart';
import '../../repository/auth_repository.dart';

class RegisterUsecase {

  RegisterUsecase({required this.authRepository});

  final AuthRepository authRepository;

  Future<Either<AppException, Unit>> call({required String password, required String email,required String passwordRepeat}) async {

    try {
      await authRepository.signUp(email: email, password: password);
      return right(unit);
    } on FirebaseAuthException catch (e) {
      if (e.code == firebaseEmailInUseCode) {
        return left(AppException.emailAlreadyTaken());
      }
      return left(AppException.unknownError(e.message.toString()));
    }
  }

}