
import 'package:dartz/dartz.dart';

import '../../../common/exception.dart';
import '../../repository/auth_repository.dart';

class SignOutUseCase {

  SignOutUseCase({required this.authRepository});

  final AuthRepository authRepository;

  Future<Either<AppException, Unit>> call() async {
    try {
      authRepository.signOut();
      return right(unit);
    } catch(_) {
      return left(AppException.unknownError('Unknown exception'));
    }
  }

}