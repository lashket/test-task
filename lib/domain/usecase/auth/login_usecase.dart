import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../../common/exception.dart';
import '../../constants.dart';
import '../../repository/auth_repository.dart';

class LoginUsecase {
  LoginUsecase({required this.authRepository});

  final AuthRepository authRepository;

  Future<Either<AppException, Unit>> call({required String password, required String email}) async {
    try {
      await authRepository.signIn(email: email, password: password);
      return right(unit);
    } on FirebaseAuthException catch (e) {
      if (e.code == firebaseWrongPasswordCode || e.code == firebaseNotFoundCode) {
        return left(AppException.invalidCredentials());
      }
      return left(AppException.unknownError(e.message.toString()));
    }
  }
}
