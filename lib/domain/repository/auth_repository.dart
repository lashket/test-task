import '../entity/user_profile.dart';

abstract class AuthRepository {

  Future<void> signIn({required String email, required String password});

  Future<void> signUp({required String email, required String password});

  Stream<UserProfile?> userProfileStream();

  Future<void> signOut();

}