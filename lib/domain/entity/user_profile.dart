class UserProfile {

  UserProfile({required this.id, this.name = '-', this.location = '-', this.imageLink, this.email = '-', this.completedProjects = 0, this.phoneNumber = '-'});

  final String id;
  final String? name;
  final String location;
  final String? imageLink;
  final String? phoneNumber;
  final String? email;
  final int completedProjects;
}