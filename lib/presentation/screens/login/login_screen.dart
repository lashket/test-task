import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../../../common/input_controller.dart';
import '../../../domain/usecase/auth/login_usecase.dart';
import '../../../generated/l10n.dart';
import '../../../providers/auth_input_controllers.dart';
import '../../../providers/usecase_providers.dart';
import '../../components/auth_footer.dart';
import '../../components/auth_input.dart';
import '../../components/buttons.dart';
import '../../components/loadable_widget.dart';
import '../../components/social_buttons.dart';
import '../../theme/assets.dart';
import '../register/register_screen.dart';
import 'provider/login_screen_state.dart';
import 'provider/login_screen_state_notifier.dart';


final StateNotifierProvider<LoginScreenStateNotifier, LoginScreenState> loginScreenStateNotifierProvider =
StateNotifierProvider<LoginScreenStateNotifier, LoginScreenState>((StateNotifierProviderRef<LoginScreenStateNotifier, LoginScreenState> ref) {
  final LoginUsecase usecase = ref.watch(loginUseCaseProvider);
  return LoginScreenStateNotifier(loginUsecase: usecase, ref: ref);
});

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  static const String path = '/auth';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).signIn),
      ),
      body: const _LoginScreenBody(),
    );
  }
}

class _LoginScreenBody extends StatelessWidget {
  const _LoginScreenBody();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: CustomScrollView(
          slivers: <Widget>[
            SliverPadding(
              padding: const EdgeInsets.only(top: 43),
              sliver: SliverToBoxAdapter(
                child: Image.asset(
                  AppIcons.signInIllustration,
                  height: 145,
                ),
              ),
            ),
            const SliverToBoxAdapter(
                child: SizedBox(
              height: 45,
            )),
            const SliverToBoxAdapter(child: _LoginForm()),
            SliverFillRemaining(
              fillOverscroll: true,
              hasScrollBody: false,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: AuthFooter(
                  onActionClick: () {
                    context.push(RegisterScreen.path);
                  },
                  title: S.of(context).dontHaveAnAccount,
                  actionTitle: S.of(context).signUp,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _LoginForm extends ConsumerWidget {
  const _LoginForm();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final InputController password = ref.watch(inputControllerStateNotifierProvider(AuthInputType.paswwordLogin));
    final InputController email = ref.watch(inputControllerStateNotifierProvider(AuthInputType.emailLogin));
    final  LoginScreenState state = ref.watch(loginScreenStateNotifierProvider);
    return Column(
      children: <Widget>[
        AuthInput(
          controller: email,
          hintText: S.of(context).username,
          textInputType: TextInputType.emailAddress,
        ),
        const SizedBox(
          height: 16,
        ),
        AuthInput(
          controller: password,
          hintText: S.of(context).password,
          obscureText: true,
          textInputType: TextInputType.visiblePassword,
        ),
        const SizedBox(
          height: 14,
        ),
        Align(alignment: Alignment.centerRight, child: GestureDetector(child: Text(S.of(context).forgotPassword))),
        const SizedBox(
          height: 50,
        ),
        LoadableWidget(
          loading: state is LoginScreenLoading,
          child: AppButton(type: ButtonType.filled).create(title: S.of(context).login, onPressed: () {
            ref
                .read(loginScreenStateNotifierProvider.notifier)
                .login(
                password: password.controller.value.text,
                email: email.controller.value.text);
          }, context: context),
        ),
        const SizedBox(
          height: 16,
        ),
        Text(S.of(context).or),
        const SizedBox(
          height: 16,
        ),
        const SocialLoginButtons(),
      ],
    );
  }
}
