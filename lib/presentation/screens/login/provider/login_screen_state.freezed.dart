// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'login_screen_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$LoginScreenState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function(String? message) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function(String? message)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function(String? message)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginScreenInitial value) initial,
    required TResult Function(LoginScreenLoading value) loading,
    required TResult Function(LoginScreenSuccess value) success,
    required TResult Function(LoginScreenError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginScreenInitial value)? initial,
    TResult? Function(LoginScreenLoading value)? loading,
    TResult? Function(LoginScreenSuccess value)? success,
    TResult? Function(LoginScreenError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginScreenInitial value)? initial,
    TResult Function(LoginScreenLoading value)? loading,
    TResult Function(LoginScreenSuccess value)? success,
    TResult Function(LoginScreenError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginScreenStateCopyWith<$Res> {
  factory $LoginScreenStateCopyWith(
          LoginScreenState value, $Res Function(LoginScreenState) then) =
      _$LoginScreenStateCopyWithImpl<$Res, LoginScreenState>;
}

/// @nodoc
class _$LoginScreenStateCopyWithImpl<$Res, $Val extends LoginScreenState>
    implements $LoginScreenStateCopyWith<$Res> {
  _$LoginScreenStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoginScreenInitialCopyWith<$Res> {
  factory _$$LoginScreenInitialCopyWith(_$LoginScreenInitial value,
          $Res Function(_$LoginScreenInitial) then) =
      __$$LoginScreenInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoginScreenInitialCopyWithImpl<$Res>
    extends _$LoginScreenStateCopyWithImpl<$Res, _$LoginScreenInitial>
    implements _$$LoginScreenInitialCopyWith<$Res> {
  __$$LoginScreenInitialCopyWithImpl(
      _$LoginScreenInitial _value, $Res Function(_$LoginScreenInitial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoginScreenInitial implements LoginScreenInitial {
  _$LoginScreenInitial();

  @override
  String toString() {
    return 'LoginScreenState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoginScreenInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function(String? message) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function(String? message)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function(String? message)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginScreenInitial value) initial,
    required TResult Function(LoginScreenLoading value) loading,
    required TResult Function(LoginScreenSuccess value) success,
    required TResult Function(LoginScreenError value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginScreenInitial value)? initial,
    TResult? Function(LoginScreenLoading value)? loading,
    TResult? Function(LoginScreenSuccess value)? success,
    TResult? Function(LoginScreenError value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginScreenInitial value)? initial,
    TResult Function(LoginScreenLoading value)? loading,
    TResult Function(LoginScreenSuccess value)? success,
    TResult Function(LoginScreenError value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class LoginScreenInitial implements LoginScreenState {
  factory LoginScreenInitial() = _$LoginScreenInitial;
}

/// @nodoc
abstract class _$$LoginScreenLoadingCopyWith<$Res> {
  factory _$$LoginScreenLoadingCopyWith(_$LoginScreenLoading value,
          $Res Function(_$LoginScreenLoading) then) =
      __$$LoginScreenLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoginScreenLoadingCopyWithImpl<$Res>
    extends _$LoginScreenStateCopyWithImpl<$Res, _$LoginScreenLoading>
    implements _$$LoginScreenLoadingCopyWith<$Res> {
  __$$LoginScreenLoadingCopyWithImpl(
      _$LoginScreenLoading _value, $Res Function(_$LoginScreenLoading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoginScreenLoading implements LoginScreenLoading {
  _$LoginScreenLoading();

  @override
  String toString() {
    return 'LoginScreenState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoginScreenLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function(String? message) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function(String? message)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function(String? message)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginScreenInitial value) initial,
    required TResult Function(LoginScreenLoading value) loading,
    required TResult Function(LoginScreenSuccess value) success,
    required TResult Function(LoginScreenError value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginScreenInitial value)? initial,
    TResult? Function(LoginScreenLoading value)? loading,
    TResult? Function(LoginScreenSuccess value)? success,
    TResult? Function(LoginScreenError value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginScreenInitial value)? initial,
    TResult Function(LoginScreenLoading value)? loading,
    TResult Function(LoginScreenSuccess value)? success,
    TResult Function(LoginScreenError value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoginScreenLoading implements LoginScreenState {
  factory LoginScreenLoading() = _$LoginScreenLoading;
}

/// @nodoc
abstract class _$$LoginScreenSuccessCopyWith<$Res> {
  factory _$$LoginScreenSuccessCopyWith(_$LoginScreenSuccess value,
          $Res Function(_$LoginScreenSuccess) then) =
      __$$LoginScreenSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoginScreenSuccessCopyWithImpl<$Res>
    extends _$LoginScreenStateCopyWithImpl<$Res, _$LoginScreenSuccess>
    implements _$$LoginScreenSuccessCopyWith<$Res> {
  __$$LoginScreenSuccessCopyWithImpl(
      _$LoginScreenSuccess _value, $Res Function(_$LoginScreenSuccess) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoginScreenSuccess implements LoginScreenSuccess {
  _$LoginScreenSuccess();

  @override
  String toString() {
    return 'LoginScreenState.success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoginScreenSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function(String? message) error,
  }) {
    return success();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function(String? message)? error,
  }) {
    return success?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function(String? message)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginScreenInitial value) initial,
    required TResult Function(LoginScreenLoading value) loading,
    required TResult Function(LoginScreenSuccess value) success,
    required TResult Function(LoginScreenError value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginScreenInitial value)? initial,
    TResult? Function(LoginScreenLoading value)? loading,
    TResult? Function(LoginScreenSuccess value)? success,
    TResult? Function(LoginScreenError value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginScreenInitial value)? initial,
    TResult Function(LoginScreenLoading value)? loading,
    TResult Function(LoginScreenSuccess value)? success,
    TResult Function(LoginScreenError value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class LoginScreenSuccess implements LoginScreenState {
  factory LoginScreenSuccess() = _$LoginScreenSuccess;
}

/// @nodoc
abstract class _$$LoginScreenErrorCopyWith<$Res> {
  factory _$$LoginScreenErrorCopyWith(
          _$LoginScreenError value, $Res Function(_$LoginScreenError) then) =
      __$$LoginScreenErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$LoginScreenErrorCopyWithImpl<$Res>
    extends _$LoginScreenStateCopyWithImpl<$Res, _$LoginScreenError>
    implements _$$LoginScreenErrorCopyWith<$Res> {
  __$$LoginScreenErrorCopyWithImpl(
      _$LoginScreenError _value, $Res Function(_$LoginScreenError) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$LoginScreenError(
      freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$LoginScreenError implements LoginScreenError {
  _$LoginScreenError(this.message);

  @override
  final String? message;

  @override
  String toString() {
    return 'LoginScreenState.error(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginScreenError &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginScreenErrorCopyWith<_$LoginScreenError> get copyWith =>
      __$$LoginScreenErrorCopyWithImpl<_$LoginScreenError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function(String? message) error,
  }) {
    return error(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function(String? message)? error,
  }) {
    return error?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function(String? message)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginScreenInitial value) initial,
    required TResult Function(LoginScreenLoading value) loading,
    required TResult Function(LoginScreenSuccess value) success,
    required TResult Function(LoginScreenError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoginScreenInitial value)? initial,
    TResult? Function(LoginScreenLoading value)? loading,
    TResult? Function(LoginScreenSuccess value)? success,
    TResult? Function(LoginScreenError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginScreenInitial value)? initial,
    TResult Function(LoginScreenLoading value)? loading,
    TResult Function(LoginScreenSuccess value)? success,
    TResult Function(LoginScreenError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class LoginScreenError implements LoginScreenState {
  factory LoginScreenError(final String? message) = _$LoginScreenError;

  String? get message;
  @JsonKey(ignore: true)
  _$$LoginScreenErrorCopyWith<_$LoginScreenError> get copyWith =>
      throw _privateConstructorUsedError;
}
