import 'package:dartz/dartz.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../common/exception.dart';
import '../../../../domain/usecase/auth/login_usecase.dart';
import '../../../../generated/l10n.dart';
import '../../../../providers/auth_input_controllers.dart';
import '../../../utils/mixins.dart';
import 'login_screen_state.dart';

class LoginScreenStateNotifier extends StateNotifier<LoginScreenState> with FieldValidator, PasswordMatchingValidator {
  LoginScreenStateNotifier({required this.loginUsecase, required this.ref}) : super(LoginScreenInitial());

  final LoginUsecase loginUsecase;
  final Ref ref;

  Future<void> login({required String password, required String email}) async {
    final bool dataValid = validateFields(ref, <AuthInputType>[AuthInputType.paswwordLogin, AuthInputType.emailLogin]);
    if (!dataValid) {
      return;
    }

    state = LoginScreenLoading();

    final Either<AppException, Unit> result = await loginUsecase(email: email, password: password);

    result.fold((AppException l) {
      if (l is InvalidCredentials) {
        setErrorForField(ref, AuthInputType.emailLogin, S.current.emailOrPasswordIsIncorrect);
      }
      if (l is UnknownError) {
        setErrorForField(ref, AuthInputType.emailLogin, l.message);
      }
      state = LoginScreenError(null);
    }, (Unit r) => state = LoginScreenSuccess());
  }
}
