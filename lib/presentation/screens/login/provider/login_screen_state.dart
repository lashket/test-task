import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_screen_state.freezed.dart';

@freezed
class LoginScreenState with _$LoginScreenState {
  factory LoginScreenState.initial() = LoginScreenInitial;

  factory LoginScreenState.loading() = LoginScreenLoading;

  factory LoginScreenState.success() = LoginScreenSuccess;

  factory LoginScreenState.error(String? message) = LoginScreenError;
}
