import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../../../common/input_controller.dart';
import '../../../domain/usecase/auth/register_usecase.dart';
import '../../../generated/l10n.dart';
import '../../../providers/auth_input_controllers.dart';
import '../../../providers/usecase_providers.dart';
import '../../components/auth_footer.dart';
import '../../components/auth_input.dart';
import '../../components/buttons.dart';
import '../../components/loadable_widget.dart';
import '../../components/social_buttons.dart';
import '../../theme/app_bar_icons.dart';
import '../../theme/assets.dart';
import '../../theme/colors.dart';
import 'provider/register_screen_state.dart';
import 'provider/register_screen_state_notifier.dart';

final StateNotifierProvider<RegisterScreenStateNotifier, RegisterScreenState> registerScreenStateNotifierProvider =
    StateNotifierProvider<RegisterScreenStateNotifier, RegisterScreenState>((StateNotifierProviderRef<RegisterScreenStateNotifier, RegisterScreenState> ref) {
  final RegisterUsecase usecase = ref.watch(registerUseCaseProvider);
  return RegisterScreenStateNotifier(registerUsecase: usecase, ref: ref);
});

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({super.key});

  static const String path = '/auth/register';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).signUp),
        leading: IconButton(
          icon: AppBarIcons.backIcon,
          onPressed: () {
            context.pop();
          },
          color: AppColors.primaryText,
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: AppBarIcons.menuIcon,
            color: AppColors.primaryText,
          )
        ],
      ),
      body: const _RegisterScreenBody(),
    );
  }
}

class _RegisterScreenBody extends StatelessWidget {
  const _RegisterScreenBody();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: CustomScrollView(
          slivers: <Widget>[
            SliverPadding(
              padding: const EdgeInsets.only(top: 43),
              sliver: SliverToBoxAdapter(
                child: Image.asset(
                  AppIcons.signUpIllustration,
                  height: 145,
                ),
              ),
            ),
            const SliverToBoxAdapter(
                child: SizedBox(
              height: 45,
            )),
            const SliverToBoxAdapter(child: _RegisterForm()),
            SliverFillRemaining(
              fillOverscroll: true,
              hasScrollBody: false,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: AuthFooter(
                  onActionClick: () {
                    context.pop();
                  },
                  title: S.of(context).alreadyHaveAnAccount,
                  actionTitle: S.of(context).signIn,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _RegisterForm extends ConsumerWidget {
  const _RegisterForm();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final InputController password = ref.watch(inputControllerStateNotifierProvider(AuthInputType.passwordRegister));
    final InputController passwordRepeat = ref.watch(inputControllerStateNotifierProvider(AuthInputType.passwordRepeat));
    final InputController email = ref.watch(inputControllerStateNotifierProvider(AuthInputType.emailRegister));
    final RegisterScreenState state = ref.watch(registerScreenStateNotifierProvider);
    return Column(
      children: <Widget>[
        AuthInput(
          controller: email,
          hintText: S.of(context).email,
          textInputType: TextInputType.emailAddress,
        ),
        const SizedBox(
          height: 16,
        ),
        AuthInput(
          controller: password,
          hintText: S.of(context).password,
          obscureText: true,
          textInputType: TextInputType.visiblePassword,
        ),
        const SizedBox(
          height: 16,
        ),
        AuthInput(
          controller: passwordRepeat,
          hintText: S.of(context).confirmPassword,
          obscureText: true,
          textInputType: TextInputType.visiblePassword,
        ),
        const SizedBox(
          height: 16,
        ),
        LoadableWidget(
          loading: state is RegisterScreenLoading,
          child: AppButton(type: ButtonType.filled).create(
              title: S.of(context).signUp,
              onPressed: () {
                ref
                    .read(registerScreenStateNotifierProvider.notifier)
                    .register(
                    password: password.controller.value.text,
                    email: email.controller.value.text,
                    passwordRepeat: passwordRepeat.controller.value.text);
              },
              context: context),
        ),
        const SizedBox(
          height: 16,
        ),
        Text(S.of(context).or),
        const SizedBox(
          height: 16,
        ),
        const SocialLoginButtons(),
      ],
    );
  }
}
