import 'package:dartz/dartz.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../common/exception.dart';
import '../../../../domain/usecase/auth/register_usecase.dart';
import '../../../../generated/l10n.dart';
import '../../../../providers/auth_input_controllers.dart';
import '../../../utils/mixins.dart';
import 'register_screen_state.dart';

class RegisterScreenStateNotifier extends StateNotifier<RegisterScreenState> with FieldValidator, PasswordMatchingValidator {
  RegisterScreenStateNotifier({required this.registerUsecase, required this.ref}) : super(RegisterScreenInitial());

  final RegisterUsecase registerUsecase;
  final Ref ref;

  Future<void> register({required String password, required String email, required String passwordRepeat}) async {

    final bool dataValid =
        validateFields(ref, <AuthInputType>[AuthInputType.passwordRegister, AuthInputType.emailRegister]) && validatePasswordsMatching(ref, password, passwordRepeat);
    if (!dataValid) {
      return;
    }

    state = RegisterScreenLoading();

    final Either<AppException, Unit> result = await registerUsecase(email: email, password: password, passwordRepeat: passwordRepeat);

    result.fold((AppException l) {
      if(l is EmailAlreadyTaken) {
        setErrorForField(ref, AuthInputType.emailRegister, S.current.emailAlreadyTaken);
      }
      if(l is UnknownError) {
        setErrorForField(ref, AuthInputType.emailRegister, l.message);
      }
      state = RegisterScreenError(null);
    }, (Unit r) => state = RegisterScreenSuccess());
  }
}
