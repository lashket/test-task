import 'package:freezed_annotation/freezed_annotation.dart';

part 'register_screen_state.freezed.dart';

@freezed
class RegisterScreenState with _$RegisterScreenState {
  factory RegisterScreenState.initial() = RegisterScreenInitial;

  factory RegisterScreenState.loading() = RegisterScreenLoading;

  factory RegisterScreenState.success() = RegisterScreenSuccess;

  factory RegisterScreenState.error(String? message) = RegisterScreenError;
}
