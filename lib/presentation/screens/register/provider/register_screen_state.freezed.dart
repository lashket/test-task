// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'register_screen_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$RegisterScreenState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function(String? message) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function(String? message)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function(String? message)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RegisterScreenInitial value) initial,
    required TResult Function(RegisterScreenLoading value) loading,
    required TResult Function(RegisterScreenSuccess value) success,
    required TResult Function(RegisterScreenError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RegisterScreenInitial value)? initial,
    TResult? Function(RegisterScreenLoading value)? loading,
    TResult? Function(RegisterScreenSuccess value)? success,
    TResult? Function(RegisterScreenError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RegisterScreenInitial value)? initial,
    TResult Function(RegisterScreenLoading value)? loading,
    TResult Function(RegisterScreenSuccess value)? success,
    TResult Function(RegisterScreenError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegisterScreenStateCopyWith<$Res> {
  factory $RegisterScreenStateCopyWith(
          RegisterScreenState value, $Res Function(RegisterScreenState) then) =
      _$RegisterScreenStateCopyWithImpl<$Res, RegisterScreenState>;
}

/// @nodoc
class _$RegisterScreenStateCopyWithImpl<$Res, $Val extends RegisterScreenState>
    implements $RegisterScreenStateCopyWith<$Res> {
  _$RegisterScreenStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$RegisterScreenInitialCopyWith<$Res> {
  factory _$$RegisterScreenInitialCopyWith(_$RegisterScreenInitial value,
          $Res Function(_$RegisterScreenInitial) then) =
      __$$RegisterScreenInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RegisterScreenInitialCopyWithImpl<$Res>
    extends _$RegisterScreenStateCopyWithImpl<$Res, _$RegisterScreenInitial>
    implements _$$RegisterScreenInitialCopyWith<$Res> {
  __$$RegisterScreenInitialCopyWithImpl(_$RegisterScreenInitial _value,
      $Res Function(_$RegisterScreenInitial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$RegisterScreenInitial implements RegisterScreenInitial {
  _$RegisterScreenInitial();

  @override
  String toString() {
    return 'RegisterScreenState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$RegisterScreenInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function(String? message) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function(String? message)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function(String? message)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RegisterScreenInitial value) initial,
    required TResult Function(RegisterScreenLoading value) loading,
    required TResult Function(RegisterScreenSuccess value) success,
    required TResult Function(RegisterScreenError value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RegisterScreenInitial value)? initial,
    TResult? Function(RegisterScreenLoading value)? loading,
    TResult? Function(RegisterScreenSuccess value)? success,
    TResult? Function(RegisterScreenError value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RegisterScreenInitial value)? initial,
    TResult Function(RegisterScreenLoading value)? loading,
    TResult Function(RegisterScreenSuccess value)? success,
    TResult Function(RegisterScreenError value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class RegisterScreenInitial implements RegisterScreenState {
  factory RegisterScreenInitial() = _$RegisterScreenInitial;
}

/// @nodoc
abstract class _$$RegisterScreenLoadingCopyWith<$Res> {
  factory _$$RegisterScreenLoadingCopyWith(_$RegisterScreenLoading value,
          $Res Function(_$RegisterScreenLoading) then) =
      __$$RegisterScreenLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RegisterScreenLoadingCopyWithImpl<$Res>
    extends _$RegisterScreenStateCopyWithImpl<$Res, _$RegisterScreenLoading>
    implements _$$RegisterScreenLoadingCopyWith<$Res> {
  __$$RegisterScreenLoadingCopyWithImpl(_$RegisterScreenLoading _value,
      $Res Function(_$RegisterScreenLoading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$RegisterScreenLoading implements RegisterScreenLoading {
  _$RegisterScreenLoading();

  @override
  String toString() {
    return 'RegisterScreenState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$RegisterScreenLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function(String? message) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function(String? message)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function(String? message)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RegisterScreenInitial value) initial,
    required TResult Function(RegisterScreenLoading value) loading,
    required TResult Function(RegisterScreenSuccess value) success,
    required TResult Function(RegisterScreenError value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RegisterScreenInitial value)? initial,
    TResult? Function(RegisterScreenLoading value)? loading,
    TResult? Function(RegisterScreenSuccess value)? success,
    TResult? Function(RegisterScreenError value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RegisterScreenInitial value)? initial,
    TResult Function(RegisterScreenLoading value)? loading,
    TResult Function(RegisterScreenSuccess value)? success,
    TResult Function(RegisterScreenError value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class RegisterScreenLoading implements RegisterScreenState {
  factory RegisterScreenLoading() = _$RegisterScreenLoading;
}

/// @nodoc
abstract class _$$RegisterScreenSuccessCopyWith<$Res> {
  factory _$$RegisterScreenSuccessCopyWith(_$RegisterScreenSuccess value,
          $Res Function(_$RegisterScreenSuccess) then) =
      __$$RegisterScreenSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RegisterScreenSuccessCopyWithImpl<$Res>
    extends _$RegisterScreenStateCopyWithImpl<$Res, _$RegisterScreenSuccess>
    implements _$$RegisterScreenSuccessCopyWith<$Res> {
  __$$RegisterScreenSuccessCopyWithImpl(_$RegisterScreenSuccess _value,
      $Res Function(_$RegisterScreenSuccess) _then)
      : super(_value, _then);
}

/// @nodoc

class _$RegisterScreenSuccess implements RegisterScreenSuccess {
  _$RegisterScreenSuccess();

  @override
  String toString() {
    return 'RegisterScreenState.success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$RegisterScreenSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function(String? message) error,
  }) {
    return success();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function(String? message)? error,
  }) {
    return success?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function(String? message)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RegisterScreenInitial value) initial,
    required TResult Function(RegisterScreenLoading value) loading,
    required TResult Function(RegisterScreenSuccess value) success,
    required TResult Function(RegisterScreenError value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RegisterScreenInitial value)? initial,
    TResult? Function(RegisterScreenLoading value)? loading,
    TResult? Function(RegisterScreenSuccess value)? success,
    TResult? Function(RegisterScreenError value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RegisterScreenInitial value)? initial,
    TResult Function(RegisterScreenLoading value)? loading,
    TResult Function(RegisterScreenSuccess value)? success,
    TResult Function(RegisterScreenError value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class RegisterScreenSuccess implements RegisterScreenState {
  factory RegisterScreenSuccess() = _$RegisterScreenSuccess;
}

/// @nodoc
abstract class _$$RegisterScreenErrorCopyWith<$Res> {
  factory _$$RegisterScreenErrorCopyWith(_$RegisterScreenError value,
          $Res Function(_$RegisterScreenError) then) =
      __$$RegisterScreenErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$RegisterScreenErrorCopyWithImpl<$Res>
    extends _$RegisterScreenStateCopyWithImpl<$Res, _$RegisterScreenError>
    implements _$$RegisterScreenErrorCopyWith<$Res> {
  __$$RegisterScreenErrorCopyWithImpl(
      _$RegisterScreenError _value, $Res Function(_$RegisterScreenError) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$RegisterScreenError(
      freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$RegisterScreenError implements RegisterScreenError {
  _$RegisterScreenError(this.message);

  @override
  final String? message;

  @override
  String toString() {
    return 'RegisterScreenState.error(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RegisterScreenError &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RegisterScreenErrorCopyWith<_$RegisterScreenError> get copyWith =>
      __$$RegisterScreenErrorCopyWithImpl<_$RegisterScreenError>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function(String? message) error,
  }) {
    return error(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function(String? message)? error,
  }) {
    return error?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function(String? message)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RegisterScreenInitial value) initial,
    required TResult Function(RegisterScreenLoading value) loading,
    required TResult Function(RegisterScreenSuccess value) success,
    required TResult Function(RegisterScreenError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RegisterScreenInitial value)? initial,
    TResult? Function(RegisterScreenLoading value)? loading,
    TResult? Function(RegisterScreenSuccess value)? success,
    TResult? Function(RegisterScreenError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RegisterScreenInitial value)? initial,
    TResult Function(RegisterScreenLoading value)? loading,
    TResult Function(RegisterScreenSuccess value)? success,
    TResult Function(RegisterScreenError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class RegisterScreenError implements RegisterScreenState {
  factory RegisterScreenError(final String? message) = _$RegisterScreenError;

  String? get message;
  @JsonKey(ignore: true)
  _$$RegisterScreenErrorCopyWith<_$RegisterScreenError> get copyWith =>
      throw _privateConstructorUsedError;
}
