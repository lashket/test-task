import 'package:flutter/material.dart';

import '../../../theme/colors.dart';
import '../../../utils/extensions.dart';
import '../models/profile_info_data.dart';

class ProfileInfoTile extends StatelessWidget {
  const ProfileInfoTile({super.key, required this.data});

  final ProfileTileData data;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 19),
      decoration: BoxDecoration(border: Border.all(color: AppColors.buttonBorderColor.withOpacity(0.3))),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 72,
            child: data.iconData,
          ),
          Container(
            height: 42,
            width: 1,
            color: AppColors.buttonBorderColor.withOpacity(0.3),
          ),
          const SizedBox(
            width: 22,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                data.type,
                style: Theme.of(context).textTheme.profileTileTitle,
              ),
              Text(
                data.displayedData,
                style: Theme.of(context).textTheme.bodyText2?.copyWith(color: Colors.white),
              )
            ],
          )
        ],
      ),
    );
  }
}
