import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../domain/entity/user_profile.dart';
import '../../../domain/usecase/auth/sign_out_usecase.dart';
import '../../../generated/l10n.dart';
import '../../../providers/usecase_providers.dart';
import '../../components/buttons.dart';
import '../../components/loadable_widget.dart';
import '../../theme/app_bar_icons.dart';
import '../../theme/colors.dart';
import '../../utils/extensions.dart';
import 'models/profile_info_data.dart';
import 'provider/profile_screen_state.dart';
import 'provider/profile_screen_state_notifier.dart';
import 'widgets/profile_info_tile.dart';

final StateNotifierProvider<ProfileScreenStateNotifier, ProfileScreenState> profileScreenStateNotifierProvider =
    StateNotifierProvider<ProfileScreenStateNotifier, ProfileScreenState>((StateNotifierProviderRef<ProfileScreenStateNotifier, ProfileScreenState> ref) {
  final SignOutUseCase usecase = ref.watch(signOutUseCaseProvider);
  return ProfileScreenStateNotifier(signOutUseCase: usecase);
});

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  static const String path = '/';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: AppBarIcons.backIcon,
          onPressed: () {},
          color: AppColors.primaryText,
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: AppBarIcons.menuIcon,
            color: AppColors.primaryText,
          )
        ],
      ),
      body: const _ProfileWidget(),
    );
  }
}

class _ProfileWidget extends ConsumerWidget {
  const _ProfileWidget();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final AsyncValue<UserProfile?> userStream = ref.watch(userStreamProvider);
    return userStream.when(
        data: (UserProfile? profile) => profile == null
            ? Container()
            : _ProfileBody(
                profile: profile,
              ),
        error: (Object err, StackTrace trace) => Center(child: Text(err.toString())),
        loading: () => const Center(child: CircularProgressIndicator()));
  }
}

class _ProfileBody extends ConsumerWidget {
  const _ProfileBody({required this.profile});

  final UserProfile profile;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final ProfileScreenState state = ref.watch(profileScreenStateNotifierProvider);
    return CustomScrollView(
      slivers: <Widget>[
        SliverPadding(
          padding: const EdgeInsets.only(top: 40, bottom: 22),
          sliver: SliverToBoxAdapter(
            child: CircleAvatar(
              backgroundImage: NetworkImage(profile.imageLink ?? ''),
              backgroundColor: Colors.amber,
              radius: 62.5,
            ),
          ),
        ),
        SliverToBoxAdapter(
          child: Align(
              child: Text(
            profile.name ?? '',
            style: Theme.of(context).textTheme.headline6,
          )),
        ),
        SliverPadding(
          padding: const EdgeInsets.only(top: 10, bottom: 17),
          sliver: SliverToBoxAdapter(
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Text(
                  profile.location,
                  style: Theme.of(context).textTheme.bodyText1,
                  textAlign: TextAlign.end,
                )),
                const SizedBox(
                  width: 13,
                ),
                Text(
                  '●',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(
                  width: 13,
                ),
                Expanded(
                    child: Text(
                  'ID: ${profile.id}',
                  style: Theme.of(context).textTheme.bodyText1,
                ))
              ],
            ),
          ),
        ),
        SliverToBoxAdapter(
          child: GestureDetector(
            onTap: () {},
            child: Align(
                child: Text(
              S.of(context).edit,
              style: Theme.of(context).textTheme.linkTheme,
            )),
          ),
        ),
        SliverPadding(
          padding: const EdgeInsets.only(left: 30, right: 30, top: 24, bottom: 40),
          sliver: SliverToBoxAdapter(
            child: Row(
              children: <Widget>[
                Expanded(child: AppButton(type: ButtonType.outlined).create(title: S.of(context).aboutMe, onPressed: () {}, context: context)),
                const SizedBox(
                  width: 15,
                ),
                Expanded(
                    child: LoadableWidget(
                  loading: state is ProfileScreenloggingOut,
                  child: AppButton(type: ButtonType.filled).create(
                      title: S.of(context).logOut,
                      onPressed: () {
                        ref.read(profileScreenStateNotifierProvider.notifier).logout();
                      },
                      context: context),
                )),
              ],
            ),
          ),
        ),
        SliverToBoxAdapter(
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 30),
            color: AppColors.profileInfoBg,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ProfileInfoTile(data: PhoneProfileData(value: profile.phoneNumber ?? '-')),
                const SizedBox(
                  height: 16,
                ),
                ProfileInfoTile(data: EmailProfileData(value: profile.email ?? '-')),
                const SizedBox(
                  height: 16,
                ),
                ProfileInfoTile(data: ProjectsProfileData(value: profile.completedProjects.toString())),
              ],
            ),
          ),
        ),
        SliverFillRemaining(
          fillOverscroll: true,
          hasScrollBody: false,
          child: Container(
            color: AppColors.profileInfoBg,
          ),
        )
      ],
    );
  }
}
