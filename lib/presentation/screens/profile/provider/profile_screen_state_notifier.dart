import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../domain/usecase/auth/sign_out_usecase.dart';
import 'profile_screen_state.dart';

class ProfileScreenStateNotifier extends StateNotifier<ProfileScreenState> {

  ProfileScreenStateNotifier({required this.signOutUseCase}) : super(ProfileScreenInitial());

  final SignOutUseCase signOutUseCase;

  Future<void> logout() async {

    state = ProfileScreenloggingOut();

    try {
      await signOutUseCase();
      state = ProfileScreenloggedOut();
    } catch(e) {
      state = ProfileScreenError();
    }

  }

}