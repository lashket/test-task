import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile_screen_state.freezed.dart';

@freezed
class ProfileScreenState with _$ProfileScreenState {
  factory ProfileScreenState.initial() = ProfileScreenInitial;
  factory ProfileScreenState.loggingOut() = ProfileScreenloggingOut;
  factory ProfileScreenState.loggedOut() = ProfileScreenloggedOut;
  factory ProfileScreenState.error() = ProfileScreenError;
}