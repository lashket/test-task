// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'profile_screen_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProfileScreenState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loggingOut,
    required TResult Function() loggedOut,
    required TResult Function() error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loggingOut,
    TResult? Function()? loggedOut,
    TResult? Function()? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loggingOut,
    TResult Function()? loggedOut,
    TResult Function()? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileScreenInitial value) initial,
    required TResult Function(ProfileScreenloggingOut value) loggingOut,
    required TResult Function(ProfileScreenloggedOut value) loggedOut,
    required TResult Function(ProfileScreenError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProfileScreenInitial value)? initial,
    TResult? Function(ProfileScreenloggingOut value)? loggingOut,
    TResult? Function(ProfileScreenloggedOut value)? loggedOut,
    TResult? Function(ProfileScreenError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileScreenInitial value)? initial,
    TResult Function(ProfileScreenloggingOut value)? loggingOut,
    TResult Function(ProfileScreenloggedOut value)? loggedOut,
    TResult Function(ProfileScreenError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileScreenStateCopyWith<$Res> {
  factory $ProfileScreenStateCopyWith(
          ProfileScreenState value, $Res Function(ProfileScreenState) then) =
      _$ProfileScreenStateCopyWithImpl<$Res, ProfileScreenState>;
}

/// @nodoc
class _$ProfileScreenStateCopyWithImpl<$Res, $Val extends ProfileScreenState>
    implements $ProfileScreenStateCopyWith<$Res> {
  _$ProfileScreenStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ProfileScreenInitialCopyWith<$Res> {
  factory _$$ProfileScreenInitialCopyWith(_$ProfileScreenInitial value,
          $Res Function(_$ProfileScreenInitial) then) =
      __$$ProfileScreenInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileScreenInitialCopyWithImpl<$Res>
    extends _$ProfileScreenStateCopyWithImpl<$Res, _$ProfileScreenInitial>
    implements _$$ProfileScreenInitialCopyWith<$Res> {
  __$$ProfileScreenInitialCopyWithImpl(_$ProfileScreenInitial _value,
      $Res Function(_$ProfileScreenInitial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProfileScreenInitial implements ProfileScreenInitial {
  _$ProfileScreenInitial();

  @override
  String toString() {
    return 'ProfileScreenState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProfileScreenInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loggingOut,
    required TResult Function() loggedOut,
    required TResult Function() error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loggingOut,
    TResult? Function()? loggedOut,
    TResult? Function()? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loggingOut,
    TResult Function()? loggedOut,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileScreenInitial value) initial,
    required TResult Function(ProfileScreenloggingOut value) loggingOut,
    required TResult Function(ProfileScreenloggedOut value) loggedOut,
    required TResult Function(ProfileScreenError value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProfileScreenInitial value)? initial,
    TResult? Function(ProfileScreenloggingOut value)? loggingOut,
    TResult? Function(ProfileScreenloggedOut value)? loggedOut,
    TResult? Function(ProfileScreenError value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileScreenInitial value)? initial,
    TResult Function(ProfileScreenloggingOut value)? loggingOut,
    TResult Function(ProfileScreenloggedOut value)? loggedOut,
    TResult Function(ProfileScreenError value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class ProfileScreenInitial implements ProfileScreenState {
  factory ProfileScreenInitial() = _$ProfileScreenInitial;
}

/// @nodoc
abstract class _$$ProfileScreenloggingOutCopyWith<$Res> {
  factory _$$ProfileScreenloggingOutCopyWith(_$ProfileScreenloggingOut value,
          $Res Function(_$ProfileScreenloggingOut) then) =
      __$$ProfileScreenloggingOutCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileScreenloggingOutCopyWithImpl<$Res>
    extends _$ProfileScreenStateCopyWithImpl<$Res, _$ProfileScreenloggingOut>
    implements _$$ProfileScreenloggingOutCopyWith<$Res> {
  __$$ProfileScreenloggingOutCopyWithImpl(_$ProfileScreenloggingOut _value,
      $Res Function(_$ProfileScreenloggingOut) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProfileScreenloggingOut implements ProfileScreenloggingOut {
  _$ProfileScreenloggingOut();

  @override
  String toString() {
    return 'ProfileScreenState.loggingOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProfileScreenloggingOut);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loggingOut,
    required TResult Function() loggedOut,
    required TResult Function() error,
  }) {
    return loggingOut();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loggingOut,
    TResult? Function()? loggedOut,
    TResult? Function()? error,
  }) {
    return loggingOut?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loggingOut,
    TResult Function()? loggedOut,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (loggingOut != null) {
      return loggingOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileScreenInitial value) initial,
    required TResult Function(ProfileScreenloggingOut value) loggingOut,
    required TResult Function(ProfileScreenloggedOut value) loggedOut,
    required TResult Function(ProfileScreenError value) error,
  }) {
    return loggingOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProfileScreenInitial value)? initial,
    TResult? Function(ProfileScreenloggingOut value)? loggingOut,
    TResult? Function(ProfileScreenloggedOut value)? loggedOut,
    TResult? Function(ProfileScreenError value)? error,
  }) {
    return loggingOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileScreenInitial value)? initial,
    TResult Function(ProfileScreenloggingOut value)? loggingOut,
    TResult Function(ProfileScreenloggedOut value)? loggedOut,
    TResult Function(ProfileScreenError value)? error,
    required TResult orElse(),
  }) {
    if (loggingOut != null) {
      return loggingOut(this);
    }
    return orElse();
  }
}

abstract class ProfileScreenloggingOut implements ProfileScreenState {
  factory ProfileScreenloggingOut() = _$ProfileScreenloggingOut;
}

/// @nodoc
abstract class _$$ProfileScreenloggedOutCopyWith<$Res> {
  factory _$$ProfileScreenloggedOutCopyWith(_$ProfileScreenloggedOut value,
          $Res Function(_$ProfileScreenloggedOut) then) =
      __$$ProfileScreenloggedOutCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileScreenloggedOutCopyWithImpl<$Res>
    extends _$ProfileScreenStateCopyWithImpl<$Res, _$ProfileScreenloggedOut>
    implements _$$ProfileScreenloggedOutCopyWith<$Res> {
  __$$ProfileScreenloggedOutCopyWithImpl(_$ProfileScreenloggedOut _value,
      $Res Function(_$ProfileScreenloggedOut) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProfileScreenloggedOut implements ProfileScreenloggedOut {
  _$ProfileScreenloggedOut();

  @override
  String toString() {
    return 'ProfileScreenState.loggedOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProfileScreenloggedOut);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loggingOut,
    required TResult Function() loggedOut,
    required TResult Function() error,
  }) {
    return loggedOut();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loggingOut,
    TResult? Function()? loggedOut,
    TResult? Function()? error,
  }) {
    return loggedOut?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loggingOut,
    TResult Function()? loggedOut,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (loggedOut != null) {
      return loggedOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileScreenInitial value) initial,
    required TResult Function(ProfileScreenloggingOut value) loggingOut,
    required TResult Function(ProfileScreenloggedOut value) loggedOut,
    required TResult Function(ProfileScreenError value) error,
  }) {
    return loggedOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProfileScreenInitial value)? initial,
    TResult? Function(ProfileScreenloggingOut value)? loggingOut,
    TResult? Function(ProfileScreenloggedOut value)? loggedOut,
    TResult? Function(ProfileScreenError value)? error,
  }) {
    return loggedOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileScreenInitial value)? initial,
    TResult Function(ProfileScreenloggingOut value)? loggingOut,
    TResult Function(ProfileScreenloggedOut value)? loggedOut,
    TResult Function(ProfileScreenError value)? error,
    required TResult orElse(),
  }) {
    if (loggedOut != null) {
      return loggedOut(this);
    }
    return orElse();
  }
}

abstract class ProfileScreenloggedOut implements ProfileScreenState {
  factory ProfileScreenloggedOut() = _$ProfileScreenloggedOut;
}

/// @nodoc
abstract class _$$ProfileScreenErrorCopyWith<$Res> {
  factory _$$ProfileScreenErrorCopyWith(_$ProfileScreenError value,
          $Res Function(_$ProfileScreenError) then) =
      __$$ProfileScreenErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProfileScreenErrorCopyWithImpl<$Res>
    extends _$ProfileScreenStateCopyWithImpl<$Res, _$ProfileScreenError>
    implements _$$ProfileScreenErrorCopyWith<$Res> {
  __$$ProfileScreenErrorCopyWithImpl(
      _$ProfileScreenError _value, $Res Function(_$ProfileScreenError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProfileScreenError implements ProfileScreenError {
  _$ProfileScreenError();

  @override
  String toString() {
    return 'ProfileScreenState.error()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProfileScreenError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loggingOut,
    required TResult Function() loggedOut,
    required TResult Function() error,
  }) {
    return error();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loggingOut,
    TResult? Function()? loggedOut,
    TResult? Function()? error,
  }) {
    return error?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loggingOut,
    TResult Function()? loggedOut,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProfileScreenInitial value) initial,
    required TResult Function(ProfileScreenloggingOut value) loggingOut,
    required TResult Function(ProfileScreenloggedOut value) loggedOut,
    required TResult Function(ProfileScreenError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProfileScreenInitial value)? initial,
    TResult? Function(ProfileScreenloggingOut value)? loggingOut,
    TResult? Function(ProfileScreenloggedOut value)? loggedOut,
    TResult? Function(ProfileScreenError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProfileScreenInitial value)? initial,
    TResult Function(ProfileScreenloggingOut value)? loggingOut,
    TResult Function(ProfileScreenloggedOut value)? loggedOut,
    TResult Function(ProfileScreenError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class ProfileScreenError implements ProfileScreenState {
  factory ProfileScreenError() = _$ProfileScreenError;
}
