import 'package:flutter/material.dart';

import '../../../../generated/l10n.dart';
import '../../../theme/colors.dart';
import '../../../theme/scale3c_icons.dart';

enum  ProfileInfoTileType {
  phone, email, projects
}

abstract class ProfileTileData {

  String get type;
  Icon? get iconData;
  String get displayedData;

}

class ProfileDataValue {
  ProfileDataValue({required this.value});

  final String value;
}

class PhoneProfileData extends ProfileDataValue implements ProfileTileData {

  PhoneProfileData({required super.value});

  @override
  Icon? get iconData => const Icon(Scale3c.phone, size: 17, color: AppColors.orange,);


  @override
  String get type => S.current.phoneNumber;

  @override
  String get displayedData => value;


}

class EmailProfileData extends ProfileDataValue implements ProfileTileData {

  EmailProfileData({required super.value});

  @override
  Icon? get iconData => const Icon(Scale3c.mail, size: 16,color: AppColors.orange,);

  @override
  String get type => S.current.email;

  @override
  String get displayedData => value;


}

class ProjectsProfileData extends ProfileDataValue implements ProfileTileData {

  ProjectsProfileData({required super.value});

  @override
  Icon? get iconData => const Icon(Icons.circle, size: 22,color: AppColors.orange,);


  @override
  String get type => S.current.completedProjects;

  @override
  String get displayedData => value;


}
