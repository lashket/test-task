
import 'package:flutter/material.dart';
import 'measure_size_widget.dart';

class LoadableWidget extends StatefulWidget {
  const LoadableWidget({
    super.key,
    required this.loading,
    required this.child,
  });

  final bool loading;
  final Widget child;

  @override
  LoadableWidgetState createState() => LoadableWidgetState();
}

class LoadableWidgetState extends State<LoadableWidget> {
  double widgetHeight = 0.0;

  @override
  Widget build(BuildContext context) {
    final ProgressIndicator progressIndicator = ProgressIndicator(size: widgetHeight);
    return widget.loading
        ? Container(
        child: widgetHeight == 0.0
            ? progressIndicator
            : SizedBox.fromSize(
            size: Size.square(widgetHeight),
            child: progressIndicator))
        : MeasureSize(
        onChange: (Size size) {
          setState(() {
            widgetHeight = size.height;
          });
        },
        child: widget.child);
  }
}

class ProgressIndicator extends StatelessWidget {
  const ProgressIndicator({super.key, required this.size});

  final double size;

  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(color: Theme.of(context).colorScheme.secondary);
  }
}
