import 'package:flutter/material.dart';

import '../../common/input_controller.dart';

class AuthInput extends StatelessWidget {
  const AuthInput({super.key, required this.hintText, this.obscureText = false, required this.controller, this.textInputType = TextInputType.text});

  final String hintText;
  final bool obscureText;
  final TextInputType textInputType;
  final InputController controller;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        TextFormField(
          controller: controller.controller,
          cursorColor: Theme.of(context).colorScheme.secondary,
          style: Theme.of(context).textTheme.headline6,
          obscureText: obscureText,
          keyboardType: textInputType,
          decoration: InputDecoration(
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            filled: true,
            contentPadding: const EdgeInsets.symmetric(horizontal: 22, vertical: 20),
            hintText: hintText,
            labelStyle: Theme.of(context).textTheme.bodyText1,
            hintStyle: Theme.of(context).textTheme.bodyText1,
          ),
        ),
        if (controller.error != null)
          Align(
            alignment: AlignmentDirectional.centerStart,
              child: Text(controller.error.toString(), style: Theme.of(context).textTheme.subtitle2?.copyWith(color: Colors.red)))
        else
          const SizedBox.shrink()
      ],
    );
  }
}
