import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../theme/assets.dart';
import '../theme/colors.dart';
import 'buttons.dart';


enum SocialButtonType {
  facebook,
  twitter,
  linkedIn
}

abstract class SocialButton {
  factory SocialButton({required SocialButtonType type}) {
    switch (type) {
      case SocialButtonType.facebook:
        return FacebookButton();
      case SocialButtonType.twitter:
        return TwitterButton();
      case SocialButtonType.linkedIn:
        return LinkedInButton();
    }
  }

  Widget create({required Function() onPressed});

}

class FacebookButton implements SocialButton {
  @override
  Widget create({required Function() onPressed}) => _IconSocialButton(onPressed: onPressed, asset: AppIcons.iconFacebook);
}

class TwitterButton implements SocialButton {
  @override
  Widget create({required Function() onPressed}) => _IconSocialButton(onPressed: onPressed, asset: AppIcons.iconTwitter);
}

class LinkedInButton implements SocialButton {
  @override
  Widget create({required Function() onPressed})  => _IconSocialButton(onPressed:onPressed, asset: AppIcons.iconLinkedIn);
}

class _IconSocialButton extends StatelessWidget {
  const _IconSocialButton({required this.onPressed, required this.asset});
  final Function() onPressed;
  final String asset;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: buttonHeight,
      child: OutlinedButton(
          style: OutlinedButton.styleFrom(
              side: const BorderSide(color: AppColors.buttonBorderColor),
              shape: const RoundedRectangleBorder()
          ),
          onPressed: onPressed,
          child: SvgPicture.asset(asset)
      ),
    );
  }
}

typedef SocialChose = Function(SocialButtonType type);

class SocialLoginButtons extends StatelessWidget {

  const SocialLoginButtons({super.key, this.onSelect});

  final SocialChose? onSelect;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(child: SocialButton(type: SocialButtonType.facebook).create(onPressed: () {})),
        const SizedBox(width: 7,),
        Expanded(child: SocialButton(type: SocialButtonType.twitter).create(onPressed: () {})),
        const SizedBox(width: 7,),
        Expanded(child: SocialButton(type: SocialButtonType.linkedIn).create(onPressed: () {})),
      ],
    );
  }
}
