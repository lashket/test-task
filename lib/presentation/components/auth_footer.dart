import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import '../utils/extensions.dart';

class AuthFooter extends StatelessWidget {

  const AuthFooter({super.key, required this.onActionClick, required this.title, required this.actionTitle});

  final Function() onActionClick;
  final String title;
  final String actionTitle;

  @override
  Widget build(BuildContext context) {
    final TapGestureRecognizer recognizer = TapGestureRecognizer();
    recognizer.onTap = onActionClick;
    return RichText(
        text: TextSpan(text: '$title ', style: Theme.of(context).textTheme.bodyText2, children: <TextSpan>[
          TextSpan(text: actionTitle,
              recognizer: recognizer,
              style: Theme.of(context).textTheme.linkTheme)
        ]));
  }
}
