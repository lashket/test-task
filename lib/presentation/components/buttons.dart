import 'package:flutter/material.dart';

import '../theme/colors.dart';

const double buttonHeight = 60;

enum ButtonType { filled, outlined }

abstract class AppButton {
  factory AppButton({required ButtonType type}) {
    switch (type) {
      case ButtonType.filled:
        return ButtonFilled();
      case ButtonType.outlined:
        return ButtonOutline();
    }
  }

  Widget create({required String title, required Function() onPressed, required BuildContext context});
}

class ButtonOutline implements AppButton {
  @override
  Widget create({required String title, required Function() onPressed, required BuildContext context}) {
    return SizedBox(
      height: buttonHeight,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          side: const BorderSide(color: AppColors.buttonBorderColor),
        ),
        onPressed: onPressed,
        child: Text(
          title,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.button,
        ),
      ),
    );
  }
}

class ButtonFilled implements AppButton {
  @override
  Widget create({required String title, required Function() onPressed, required BuildContext context}) {
    return SizedBox(
      height: buttonHeight,
      width: double.infinity,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(elevation: 0, backgroundColor: AppColors.secondary, shape: const RoundedRectangleBorder()),
        child: Text(
          title,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.button?.copyWith(color: Colors.white),
        ),
      ),
    );
  }
}
