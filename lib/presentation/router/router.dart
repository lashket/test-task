import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import 'router_notifier.dart';

final GlobalKey<NavigatorState> _key = GlobalKey<NavigatorState>();

final AutoDisposeProvider<GoRouter> routerProvider = Provider.autoDispose<GoRouter>((AutoDisposeProviderRef<GoRouter> ref) {
  final ProviderSubscription<AsyncValue<bool>> sub = ref.listen(routerNotifierProvider, (_, __) {});
  ref.onDispose(sub.close);

  final RouterNotifier notifier = ref.read(routerNotifierProvider.notifier);

  return GoRouter(
    navigatorKey: _key,
    refreshListenable: notifier,
    debugLogDiagnostics: true,
    initialLocation: '/',
    routes: notifier.routes,
    redirect: notifier.redirect,
  );
});
