
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../../domain/entity/user_profile.dart';
import '../../providers/usecase_providers.dart';
import '../screens/login/login_screen.dart';
import '../screens/profile/profile_screen.dart';
import '../screens/register/register_screen.dart';

class RouterNotifier extends AutoDisposeAsyncNotifier<bool>
    implements Listenable {

  VoidCallback? routerListener;

  @override
  Future<bool> build() async {
    final bool isAuth = await ref.watch(userStreamProvider.selectAsync((UserProfile? data)  => data != null));
    ref.listenSelf((_, __) {
      if (state.isLoading) {
        return;
      }
      routerListener?.call();
    });

    return isAuth;
  }

  String? redirect(BuildContext context, GoRouterState state) {
    final bool? isAuth = this.state.valueOrNull;
    if (isAuth == null) {
      return null;
    }
    final bool isRegister = state.location ==  RegisterScreen.path;

    if (isRegister) {
      return isAuth ? ProfileScreen.path :  RegisterScreen.path;
    }

    final bool isLoggingIn = state.location ==  LoginScreen.path || state.location == RegisterScreen.path;
    if (isLoggingIn) {
      return isAuth ? ProfileScreen.path : null;
    }

    return isAuth ? null :  LoginScreen.path;
  }

  List<GoRoute> get routes => <GoRoute>[
    GoRoute(
      path: ProfileScreen.path,
      builder: (BuildContext context, GoRouterState state) => const ProfileScreen(),
    ),
    GoRoute(
      path: LoginScreen.path,
      builder: (BuildContext context, GoRouterState state) => const LoginScreen(),
    ),
    GoRoute(
      path: RegisterScreen.path,
      builder: (BuildContext context, GoRouterState state) => const RegisterScreen(),
    ),
  ];

  @override
  void addListener(VoidCallback listener) {
    routerListener = listener;
  }

  @override
  void removeListener(VoidCallback listener) {
    routerListener = null;
  }
}

final AutoDisposeAsyncNotifierProvider<RouterNotifier, bool> routerNotifierProvider =
AutoDisposeAsyncNotifierProvider<RouterNotifier, bool>(() {
  return RouterNotifier();
});
