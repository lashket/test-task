import '../utils/extensions.dart';

class AppIcons {
  AppIcons._();

  static String get signInIllustration => 'sign_in'.asIllustration().toPng();

  static String get signUpIllustration => 'sign_up'.asIllustration().toPng();

  static String get iconFacebook => 'ic_facebook'.asSocialIcon().toSvg();

  static String get iconTwitter => 'ic_twitter'.asSocialIcon().toSvg();

  static String get iconLinkedIn => 'ic_linkedin'.asSocialIcon().toSvg();

  static String get iconBack => 'ic_back'.asIcon().toSvg();

  static String get iconMenu => 'ic_menu'.asIcon().toSvg();
}
