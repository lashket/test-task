import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';

class AppTheme {
  AppTheme._();

  static ThemeData get light {
    final ThemeData theme = ThemeData();
    return ThemeData(
        colorScheme: theme.colorScheme.copyWith(
          secondary: AppColors.secondary,
          primary: AppColors.primary,
          background: AppColors.background,
        ),
        appBarTheme: AppBarTheme(
            elevation: 0,
            centerTitle: true,
            titleTextStyle: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w900, color: AppColors.primaryText, letterSpacing: 0.38)),
        scaffoldBackgroundColor: AppColors.background,
        textTheme: TextTheme(
                headline1: GoogleFonts.ubuntu(fontSize: 93, fontWeight: FontWeight.w300, letterSpacing: -1.5),
                headline2: GoogleFonts.ubuntu(fontSize: 58, fontWeight: FontWeight.w300, letterSpacing: -0.5),
                headline3: GoogleFonts.ubuntu(fontSize: 46, fontWeight: FontWeight.w400),
                headline4: GoogleFonts.ubuntu(fontSize: 33, fontWeight: FontWeight.w400, letterSpacing: 0.25),
                headline5: GoogleFonts.ubuntu(fontSize: 24, fontWeight: FontWeight.w400),
                headline6: GoogleFonts.ubuntu(fontSize: 19, fontWeight: FontWeight.w500, letterSpacing: 0.15),
                subtitle1: GoogleFonts.ubuntu(fontSize: 15, fontWeight: FontWeight.w400, letterSpacing: 0.15),
                subtitle2: GoogleFonts.ubuntu(color: AppColors.primaryText, fontSize: 13, fontWeight: FontWeight.w500, letterSpacing: 0.1),
                bodyText1: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w500, letterSpacing: 0.5),
                bodyText2: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w900, letterSpacing: 0.38),
                button: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w900, letterSpacing: 0.07),
                caption: GoogleFonts.roboto(fontSize: 12, fontWeight: FontWeight.w900, letterSpacing: 0.4),
                overline: GoogleFonts.roboto(fontSize: 10, fontWeight: FontWeight.w400, letterSpacing: 0.09))
            .apply(
          bodyColor: AppColors.primaryText,
        ));
  }
}
