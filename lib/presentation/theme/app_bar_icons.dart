import 'package:flutter/material.dart';

import 'scale3c_icons.dart';

class AppBarIcons {
  AppBarIcons._();

  static const Icon backIcon = Icon(
    Scale3c.ic_back,
    size: 16,
  );
  static const Icon menuIcon = Icon(
    Scale3c.menu,
    size: 8,
  );
}
