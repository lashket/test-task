import 'package:flutter/material.dart';

class AppColors {
  const AppColors._();

  static const Color secondary = Color(0xff20C3AF);
  static const Color orange = Color(0xffFFB19D);
  static const Color primary = Color(0xffF2F2F2);
  static const Color background = Color(0xffF2F2F2);
  static const Color profileInfoBg = Color(0xff525464);

  static const Color primaryText = Color(0xff838391);
  static const Color hintColor = Color(0xffB0B0C3);
  static const Color inputBgColor = Color(0xffF7F7F7);

  static const Color buttonBorderColor = Color(0xffE2E2E0);

}
