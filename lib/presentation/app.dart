import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import '../generated/l10n.dart';
import 'router/router.dart';

import 'theme/theme.dart';

class App extends ConsumerWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final GoRouter router = ref.watch(routerProvider);
    return MaterialApp.router(
      title: 'Scale3c',
      theme: AppTheme.light,
      routerConfig: router,
      localizationsDelegates: const <LocalizationsDelegate<dynamic>>[
        S.delegate
      ],
    );
  }
}
