import 'package:flutter/material.dart';

import '../theme/colors.dart';

extension AssetExtension on String {
  String asIllustration() => 'assets/illustrations/$this';

  String asSocialIcon() => 'assets/icons/social/$this';

  String asIcon() => 'assets/icons/$this';

  String toSvg() => '$this.svg';

  String toPng() => '$this.png';
}

extension TextThemeExtensions on TextTheme {
  TextStyle? get linkTheme => bodyText2?.copyWith(
        fontWeight: FontWeight.w500,
        color: AppColors.orange,
        decoration: TextDecoration.underline,
      );

  TextStyle? get profileTileTitle => bodyText2?.copyWith(fontWeight: FontWeight.w500, color: Colors.white.withOpacity(0.381), fontSize: 14);
}
