import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../generated/l10n.dart';
import '../../providers/auth_input_controllers.dart';

mixin FieldValidator {
  bool validateFields(Ref ref, List<AuthInputType> types) {
    bool valid = true;
    for (final AuthInputType type in types) {
      final bool result = ref.read(inputControllerStateNotifierProvider(type).notifier).validate();
      if (!result) {
        valid = false;
      }
    }
    return valid;
  }

  void setErrorForField(Ref ref, AuthInputType type, String error) {
    ref.read(inputControllerStateNotifierProvider(type).notifier).updateError(error);
  }
}

mixin PasswordMatchingValidator {
  bool _checkPasswordMatching(String password, String passwordRepeat) => password == passwordRepeat;

  bool validatePasswordsMatching(Ref ref, String password, String passwordRepeat) {
    String? error;
    if (!_checkPasswordMatching(password, passwordRepeat)) {
      error = S.current.passwordsDoesntMatch;
    }
    ref.read(inputControllerStateNotifierProvider(AuthInputType.passwordRepeat).notifier).updateError(error);
    return error == null;
  }
}
