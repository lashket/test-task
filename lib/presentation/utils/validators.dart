import 'package:dartz/dartz.dart';

import '../../common/exception.dart';
import '../../common/utils/extensions.dart';
import '../../generated/l10n.dart';
import '../../providers/auth_input_controllers.dart';

const int minimumPasswordLength = 8;

abstract class Validator {

  factory Validator({required AuthInputType type}) {
    switch (type) {
      case AuthInputType.emailRegister:
      case AuthInputType.emailLogin:
        return EmailValidator();
      case AuthInputType.passwordRegister:
      case AuthInputType.paswwordLogin:
        return PasswordValidator();
      case AuthInputType.passwordRepeat:
        return PasswordValidator();
    }
  }

  Either<ValidationException, Unit> validate({required String value});

}

class EmailValidator implements Validator {
  @override
  Either<ValidationException, Unit> validate({required String value}) {
    if(value.emailValid) {
      return right(unit);
    }
    return left(ValidationException(S.current.emailIsIncorrect));
  }

}

class PasswordValidator implements Validator {

  @override
  Either<ValidationException, Unit> validate({required String value}) {
    if(value.passwordValid) {
      return right(unit);
    }
    return left(ValidationException(S.current.passwordMustBeAtLeast8Symbols(minimumPasswordLength)));
  }


}
