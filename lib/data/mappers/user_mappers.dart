import 'package:firebase_auth/firebase_auth.dart';

import '../../domain/entity/user_profile.dart';

class UserMappers {

  UserMappers._();

  static UserProfile? profileFromFirebaseUser({required User? user}) {
    if(user == null) {
      return null;
    }
    return UserProfile(id: user.uid, name: user.displayName ?? '-', email: user.email, phoneNumber: user.phoneNumber,);
  }

}
