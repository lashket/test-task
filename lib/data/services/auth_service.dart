import 'package:firebase_auth/firebase_auth.dart';

abstract class AuthService {
  Stream<User?> currentUserStream();

  User? getCurrentUser();

  Future<UserCredential> signInWithEmail({required String email, required String password});

  Future<UserCredential> signUpWithEmail({required String email, required String password});

  Future<void> logout();
}

class AuthServiceImpl implements AuthService {
  AuthServiceImpl({required this.firebaseAuth});

  final FirebaseAuth firebaseAuth;

  @override
  Stream<User?> currentUserStream() => firebaseAuth.authStateChanges();

  @override
  User? getCurrentUser() => firebaseAuth.currentUser;

  @override
  Future<UserCredential> signInWithEmail({required String email, required String password}) =>
      firebaseAuth.signInWithEmailAndPassword(email: email, password: password);

  @override
  Future<UserCredential> signUpWithEmail({required String email, required String password})  =>
      firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);

  @override
  Future<void> logout() => firebaseAuth.signOut();




}
