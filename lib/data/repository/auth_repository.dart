import 'package:firebase_auth/firebase_auth.dart';

import '../../domain/entity/user_profile.dart';
import '../../domain/repository/auth_repository.dart';
import '../mappers/user_mappers.dart';
import '../services/auth_service.dart';

class AuthRepositoryImpl implements AuthRepository {
  AuthRepositoryImpl({required this.authService});

  final AuthService authService;

  @override
  Future<void> signIn({required String email, required String password}) => authService.signInWithEmail(email: email, password: password);

  @override
  Future<void> signUp({required String email, required String password}) => authService.signUpWithEmail(email: email, password: password);

  @override
  Stream<UserProfile?> userProfileStream() => authService.currentUserStream().map((User? event) => UserMappers.profileFromFirebaseUser(user: event));

  @override
  Future<void> signOut() => authService.logout();
}
