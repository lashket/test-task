import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../common/exception.dart';
import '../common/input_controller.dart';
import '../presentation/utils/validators.dart';

enum AuthInputType { emailRegister, passwordRegister, passwordRepeat, emailLogin, paswwordLogin }

final AutoDisposeProviderFamily<TextEditingController, AuthInputType> inputControllerProvider =
    Provider.autoDispose.family<TextEditingController, AuthInputType>((AutoDisposeProviderRef<TextEditingController> ref, AuthInputType type) {
  final TextEditingController controller = TextEditingController();
  ref.onDispose(() {
    controller.dispose();
  });
  return controller;
});

final AutoDisposeStateNotifierProviderFamily<InputStateNotifier, InputController, AuthInputType> inputControllerStateNotifierProvider = StateNotifierProvider.autoDispose.family<InputStateNotifier, InputController, AuthInputType>((AutoDisposeStateNotifierProviderRef<InputStateNotifier, InputController> ref, AuthInputType type) {
  final TextEditingController controller = TextEditingController();
  ref.onDispose(() {
    controller.dispose();
  });
  return InputStateNotifier(type: type, controller: controller, validator: Validator(type: type));
});

class InputStateNotifier extends StateNotifier<InputController> {
  InputStateNotifier({required this.type, required this.controller, required this.validator})
      : super(InputController(controller: controller, authInputType: type, error: null));

  final AuthInputType type;
  final TextEditingController controller;
  final Validator validator;

  bool validate() {
    final String text = controller.value.text;
    final Either<ValidationException, Unit> result = validator.validate(value: text);
    return result.fold((ValidationException l) {
      updateError(l.message);
      return false;
    }, (Unit r) {
      _clearError();
      return true;
    });
  }

  void _clearError() {
    updateError(null);
  }

  void updateError(String? error) {
    state = state.copyWith(error: error);
  }
}
