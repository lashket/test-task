import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../data/services/auth_service.dart';

final Provider<AuthService> firebaseAuthProvider = Provider<AuthService>((ProviderRef<AuthService> ref) {
  final FirebaseAuth instance = FirebaseAuth.instance;
  return AuthServiceImpl(firebaseAuth: instance);
});
