import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../domain/entity/user_profile.dart';
import '../domain/repository/auth_repository.dart';
import '../domain/usecase/auth/current_user_stream_usecase.dart';
import '../domain/usecase/auth/login_usecase.dart';
import '../domain/usecase/auth/register_usecase.dart';
import '../domain/usecase/auth/sign_out_usecase.dart';
import 'repository_providers.dart';

final Provider<CurrentUserStreamUseCase> getUserStreamUseCaseProvider = Provider<CurrentUserStreamUseCase>((ProviderRef<CurrentUserStreamUseCase> ref) {
  final AuthRepository repository = ref.watch(authRepositoryProvider);
  return CurrentUserStreamUseCase(authRepository: repository);
});

final Provider<LoginUsecase> loginUseCaseProvider = Provider<LoginUsecase>((ProviderRef<LoginUsecase> ref) {
  final AuthRepository repository = ref.watch(authRepositoryProvider);
  return LoginUsecase(authRepository: repository);
});

final Provider<RegisterUsecase> registerUseCaseProvider = Provider<RegisterUsecase>((ProviderRef<RegisterUsecase> ref) {
  final AuthRepository repository = ref.watch(authRepositoryProvider);
  return RegisterUsecase(authRepository: repository);
});

final Provider<SignOutUseCase> signOutUseCaseProvider = Provider<SignOutUseCase>((ProviderRef<SignOutUseCase> ref) {
  final AuthRepository repository = ref.watch(authRepositoryProvider);
  return SignOutUseCase(authRepository: repository);
});

final AutoDisposeStreamProvider<UserProfile?> userStreamProvider = StreamProvider.autoDispose<UserProfile?>((AutoDisposeStreamProviderRef<UserProfile?> ref)  {
  final CurrentUserStreamUseCase usecase = ref.watch(getUserStreamUseCaseProvider);
  final Stream<UserProfile?> streeam = usecase();
  return streeam;
});
