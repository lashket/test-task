import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../data/repository/auth_repository.dart';
import '../data/services/auth_service.dart';
import '../domain/repository/auth_repository.dart';
import 'services_providers.dart';

final Provider<AuthRepository> authRepositoryProvider = Provider<AuthRepository>((ProviderRef<AuthRepository> ref) {
  final AuthService service = ref.watch(firebaseAuthProvider);
  return AuthRepositoryImpl(authService: service);
});
