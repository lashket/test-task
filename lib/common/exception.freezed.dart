// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'exception.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AppException {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) authException,
    required TResult Function(String message) unknownError,
    required TResult Function() invalidCredentials,
    required TResult Function() emailAlreadyTaken,
    required TResult Function(String message) validationException,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message)? authException,
    TResult? Function(String message)? unknownError,
    TResult? Function()? invalidCredentials,
    TResult? Function()? emailAlreadyTaken,
    TResult? Function(String message)? validationException,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? authException,
    TResult Function(String message)? unknownError,
    TResult Function()? invalidCredentials,
    TResult Function()? emailAlreadyTaken,
    TResult Function(String message)? validationException,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthException value) authException,
    required TResult Function(UnknownError value) unknownError,
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(EmailAlreadyTaken value) emailAlreadyTaken,
    required TResult Function(ValidationException value) validationException,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthException value)? authException,
    TResult? Function(UnknownError value)? unknownError,
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(EmailAlreadyTaken value)? emailAlreadyTaken,
    TResult? Function(ValidationException value)? validationException,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthException value)? authException,
    TResult Function(UnknownError value)? unknownError,
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(EmailAlreadyTaken value)? emailAlreadyTaken,
    TResult Function(ValidationException value)? validationException,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppExceptionCopyWith<$Res> {
  factory $AppExceptionCopyWith(
          AppException value, $Res Function(AppException) then) =
      _$AppExceptionCopyWithImpl<$Res, AppException>;
}

/// @nodoc
class _$AppExceptionCopyWithImpl<$Res, $Val extends AppException>
    implements $AppExceptionCopyWith<$Res> {
  _$AppExceptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$AuthExceptionCopyWith<$Res> {
  factory _$$AuthExceptionCopyWith(
          _$AuthException value, $Res Function(_$AuthException) then) =
      __$$AuthExceptionCopyWithImpl<$Res>;
  @useResult
  $Res call({String message});
}

/// @nodoc
class __$$AuthExceptionCopyWithImpl<$Res>
    extends _$AppExceptionCopyWithImpl<$Res, _$AuthException>
    implements _$$AuthExceptionCopyWith<$Res> {
  __$$AuthExceptionCopyWithImpl(
      _$AuthException _value, $Res Function(_$AuthException) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = null,
  }) {
    return _then(_$AuthException(
      null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$AuthException implements AuthException {
  _$AuthException(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'AppException.authException(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthException &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthExceptionCopyWith<_$AuthException> get copyWith =>
      __$$AuthExceptionCopyWithImpl<_$AuthException>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) authException,
    required TResult Function(String message) unknownError,
    required TResult Function() invalidCredentials,
    required TResult Function() emailAlreadyTaken,
    required TResult Function(String message) validationException,
  }) {
    return authException(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message)? authException,
    TResult? Function(String message)? unknownError,
    TResult? Function()? invalidCredentials,
    TResult? Function()? emailAlreadyTaken,
    TResult? Function(String message)? validationException,
  }) {
    return authException?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? authException,
    TResult Function(String message)? unknownError,
    TResult Function()? invalidCredentials,
    TResult Function()? emailAlreadyTaken,
    TResult Function(String message)? validationException,
    required TResult orElse(),
  }) {
    if (authException != null) {
      return authException(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthException value) authException,
    required TResult Function(UnknownError value) unknownError,
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(EmailAlreadyTaken value) emailAlreadyTaken,
    required TResult Function(ValidationException value) validationException,
  }) {
    return authException(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthException value)? authException,
    TResult? Function(UnknownError value)? unknownError,
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(EmailAlreadyTaken value)? emailAlreadyTaken,
    TResult? Function(ValidationException value)? validationException,
  }) {
    return authException?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthException value)? authException,
    TResult Function(UnknownError value)? unknownError,
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(EmailAlreadyTaken value)? emailAlreadyTaken,
    TResult Function(ValidationException value)? validationException,
    required TResult orElse(),
  }) {
    if (authException != null) {
      return authException(this);
    }
    return orElse();
  }
}

abstract class AuthException implements AppException {
  factory AuthException(final String message) = _$AuthException;

  String get message;
  @JsonKey(ignore: true)
  _$$AuthExceptionCopyWith<_$AuthException> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UnknownErrorCopyWith<$Res> {
  factory _$$UnknownErrorCopyWith(
          _$UnknownError value, $Res Function(_$UnknownError) then) =
      __$$UnknownErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String message});
}

/// @nodoc
class __$$UnknownErrorCopyWithImpl<$Res>
    extends _$AppExceptionCopyWithImpl<$Res, _$UnknownError>
    implements _$$UnknownErrorCopyWith<$Res> {
  __$$UnknownErrorCopyWithImpl(
      _$UnknownError _value, $Res Function(_$UnknownError) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = null,
  }) {
    return _then(_$UnknownError(
      null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$UnknownError implements UnknownError {
  _$UnknownError(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'AppException.unknownError(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnknownError &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UnknownErrorCopyWith<_$UnknownError> get copyWith =>
      __$$UnknownErrorCopyWithImpl<_$UnknownError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) authException,
    required TResult Function(String message) unknownError,
    required TResult Function() invalidCredentials,
    required TResult Function() emailAlreadyTaken,
    required TResult Function(String message) validationException,
  }) {
    return unknownError(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message)? authException,
    TResult? Function(String message)? unknownError,
    TResult? Function()? invalidCredentials,
    TResult? Function()? emailAlreadyTaken,
    TResult? Function(String message)? validationException,
  }) {
    return unknownError?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? authException,
    TResult Function(String message)? unknownError,
    TResult Function()? invalidCredentials,
    TResult Function()? emailAlreadyTaken,
    TResult Function(String message)? validationException,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthException value) authException,
    required TResult Function(UnknownError value) unknownError,
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(EmailAlreadyTaken value) emailAlreadyTaken,
    required TResult Function(ValidationException value) validationException,
  }) {
    return unknownError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthException value)? authException,
    TResult? Function(UnknownError value)? unknownError,
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(EmailAlreadyTaken value)? emailAlreadyTaken,
    TResult? Function(ValidationException value)? validationException,
  }) {
    return unknownError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthException value)? authException,
    TResult Function(UnknownError value)? unknownError,
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(EmailAlreadyTaken value)? emailAlreadyTaken,
    TResult Function(ValidationException value)? validationException,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(this);
    }
    return orElse();
  }
}

abstract class UnknownError implements AppException {
  factory UnknownError(final String message) = _$UnknownError;

  String get message;
  @JsonKey(ignore: true)
  _$$UnknownErrorCopyWith<_$UnknownError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$InvalidCredentialsCopyWith<$Res> {
  factory _$$InvalidCredentialsCopyWith(_$InvalidCredentials value,
          $Res Function(_$InvalidCredentials) then) =
      __$$InvalidCredentialsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InvalidCredentialsCopyWithImpl<$Res>
    extends _$AppExceptionCopyWithImpl<$Res, _$InvalidCredentials>
    implements _$$InvalidCredentialsCopyWith<$Res> {
  __$$InvalidCredentialsCopyWithImpl(
      _$InvalidCredentials _value, $Res Function(_$InvalidCredentials) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InvalidCredentials implements InvalidCredentials {
  _$InvalidCredentials();

  @override
  String toString() {
    return 'AppException.invalidCredentials()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InvalidCredentials);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) authException,
    required TResult Function(String message) unknownError,
    required TResult Function() invalidCredentials,
    required TResult Function() emailAlreadyTaken,
    required TResult Function(String message) validationException,
  }) {
    return invalidCredentials();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message)? authException,
    TResult? Function(String message)? unknownError,
    TResult? Function()? invalidCredentials,
    TResult? Function()? emailAlreadyTaken,
    TResult? Function(String message)? validationException,
  }) {
    return invalidCredentials?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? authException,
    TResult Function(String message)? unknownError,
    TResult Function()? invalidCredentials,
    TResult Function()? emailAlreadyTaken,
    TResult Function(String message)? validationException,
    required TResult orElse(),
  }) {
    if (invalidCredentials != null) {
      return invalidCredentials();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthException value) authException,
    required TResult Function(UnknownError value) unknownError,
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(EmailAlreadyTaken value) emailAlreadyTaken,
    required TResult Function(ValidationException value) validationException,
  }) {
    return invalidCredentials(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthException value)? authException,
    TResult? Function(UnknownError value)? unknownError,
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(EmailAlreadyTaken value)? emailAlreadyTaken,
    TResult? Function(ValidationException value)? validationException,
  }) {
    return invalidCredentials?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthException value)? authException,
    TResult Function(UnknownError value)? unknownError,
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(EmailAlreadyTaken value)? emailAlreadyTaken,
    TResult Function(ValidationException value)? validationException,
    required TResult orElse(),
  }) {
    if (invalidCredentials != null) {
      return invalidCredentials(this);
    }
    return orElse();
  }
}

abstract class InvalidCredentials implements AppException {
  factory InvalidCredentials() = _$InvalidCredentials;
}

/// @nodoc
abstract class _$$EmailAlreadyTakenCopyWith<$Res> {
  factory _$$EmailAlreadyTakenCopyWith(
          _$EmailAlreadyTaken value, $Res Function(_$EmailAlreadyTaken) then) =
      __$$EmailAlreadyTakenCopyWithImpl<$Res>;
}

/// @nodoc
class __$$EmailAlreadyTakenCopyWithImpl<$Res>
    extends _$AppExceptionCopyWithImpl<$Res, _$EmailAlreadyTaken>
    implements _$$EmailAlreadyTakenCopyWith<$Res> {
  __$$EmailAlreadyTakenCopyWithImpl(
      _$EmailAlreadyTaken _value, $Res Function(_$EmailAlreadyTaken) _then)
      : super(_value, _then);
}

/// @nodoc

class _$EmailAlreadyTaken implements EmailAlreadyTaken {
  _$EmailAlreadyTaken();

  @override
  String toString() {
    return 'AppException.emailAlreadyTaken()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$EmailAlreadyTaken);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) authException,
    required TResult Function(String message) unknownError,
    required TResult Function() invalidCredentials,
    required TResult Function() emailAlreadyTaken,
    required TResult Function(String message) validationException,
  }) {
    return emailAlreadyTaken();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message)? authException,
    TResult? Function(String message)? unknownError,
    TResult? Function()? invalidCredentials,
    TResult? Function()? emailAlreadyTaken,
    TResult? Function(String message)? validationException,
  }) {
    return emailAlreadyTaken?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? authException,
    TResult Function(String message)? unknownError,
    TResult Function()? invalidCredentials,
    TResult Function()? emailAlreadyTaken,
    TResult Function(String message)? validationException,
    required TResult orElse(),
  }) {
    if (emailAlreadyTaken != null) {
      return emailAlreadyTaken();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthException value) authException,
    required TResult Function(UnknownError value) unknownError,
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(EmailAlreadyTaken value) emailAlreadyTaken,
    required TResult Function(ValidationException value) validationException,
  }) {
    return emailAlreadyTaken(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthException value)? authException,
    TResult? Function(UnknownError value)? unknownError,
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(EmailAlreadyTaken value)? emailAlreadyTaken,
    TResult? Function(ValidationException value)? validationException,
  }) {
    return emailAlreadyTaken?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthException value)? authException,
    TResult Function(UnknownError value)? unknownError,
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(EmailAlreadyTaken value)? emailAlreadyTaken,
    TResult Function(ValidationException value)? validationException,
    required TResult orElse(),
  }) {
    if (emailAlreadyTaken != null) {
      return emailAlreadyTaken(this);
    }
    return orElse();
  }
}

abstract class EmailAlreadyTaken implements AppException {
  factory EmailAlreadyTaken() = _$EmailAlreadyTaken;
}

/// @nodoc
abstract class _$$ValidationExceptionCopyWith<$Res> {
  factory _$$ValidationExceptionCopyWith(_$ValidationException value,
          $Res Function(_$ValidationException) then) =
      __$$ValidationExceptionCopyWithImpl<$Res>;
  @useResult
  $Res call({String message});
}

/// @nodoc
class __$$ValidationExceptionCopyWithImpl<$Res>
    extends _$AppExceptionCopyWithImpl<$Res, _$ValidationException>
    implements _$$ValidationExceptionCopyWith<$Res> {
  __$$ValidationExceptionCopyWithImpl(
      _$ValidationException _value, $Res Function(_$ValidationException) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = null,
  }) {
    return _then(_$ValidationException(
      null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ValidationException implements ValidationException {
  _$ValidationException(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'AppException.validationException(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ValidationException &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ValidationExceptionCopyWith<_$ValidationException> get copyWith =>
      __$$ValidationExceptionCopyWithImpl<_$ValidationException>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String message) authException,
    required TResult Function(String message) unknownError,
    required TResult Function() invalidCredentials,
    required TResult Function() emailAlreadyTaken,
    required TResult Function(String message) validationException,
  }) {
    return validationException(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String message)? authException,
    TResult? Function(String message)? unknownError,
    TResult? Function()? invalidCredentials,
    TResult? Function()? emailAlreadyTaken,
    TResult? Function(String message)? validationException,
  }) {
    return validationException?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String message)? authException,
    TResult Function(String message)? unknownError,
    TResult Function()? invalidCredentials,
    TResult Function()? emailAlreadyTaken,
    TResult Function(String message)? validationException,
    required TResult orElse(),
  }) {
    if (validationException != null) {
      return validationException(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthException value) authException,
    required TResult Function(UnknownError value) unknownError,
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(EmailAlreadyTaken value) emailAlreadyTaken,
    required TResult Function(ValidationException value) validationException,
  }) {
    return validationException(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AuthException value)? authException,
    TResult? Function(UnknownError value)? unknownError,
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(EmailAlreadyTaken value)? emailAlreadyTaken,
    TResult? Function(ValidationException value)? validationException,
  }) {
    return validationException?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthException value)? authException,
    TResult Function(UnknownError value)? unknownError,
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(EmailAlreadyTaken value)? emailAlreadyTaken,
    TResult Function(ValidationException value)? validationException,
    required TResult orElse(),
  }) {
    if (validationException != null) {
      return validationException(this);
    }
    return orElse();
  }
}

abstract class ValidationException implements AppException {
  factory ValidationException(final String message) = _$ValidationException;

  String get message;
  @JsonKey(ignore: true)
  _$$ValidationExceptionCopyWith<_$ValidationException> get copyWith =>
      throw _privateConstructorUsedError;
}
