import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../providers/auth_input_controllers.dart';

part 'input_controller.freezed.dart';

@freezed
class InputController with _$InputController {
  const factory InputController({required TextEditingController controller, required String? error, required AuthInputType authInputType}) = _InputController;
}
