import '../../presentation/utils/validators.dart';

extension StringExtensions on String {
  bool get emailValid => RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(this);

  bool get passwordValid => length >= minimumPasswordLength;
}
