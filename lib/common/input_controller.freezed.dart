// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'input_controller.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$InputController {
  TextEditingController get controller => throw _privateConstructorUsedError;
  String? get error => throw _privateConstructorUsedError;
  AuthInputType get authInputType => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $InputControllerCopyWith<InputController> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InputControllerCopyWith<$Res> {
  factory $InputControllerCopyWith(
          InputController value, $Res Function(InputController) then) =
      _$InputControllerCopyWithImpl<$Res, InputController>;
  @useResult
  $Res call(
      {TextEditingController controller,
      String? error,
      AuthInputType authInputType});
}

/// @nodoc
class _$InputControllerCopyWithImpl<$Res, $Val extends InputController>
    implements $InputControllerCopyWith<$Res> {
  _$InputControllerCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? controller = null,
    Object? error = freezed,
    Object? authInputType = null,
  }) {
    return _then(_value.copyWith(
      controller: null == controller
          ? _value.controller
          : controller // ignore: cast_nullable_to_non_nullable
              as TextEditingController,
      error: freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String?,
      authInputType: null == authInputType
          ? _value.authInputType
          : authInputType // ignore: cast_nullable_to_non_nullable
              as AuthInputType,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_InputControllerCopyWith<$Res>
    implements $InputControllerCopyWith<$Res> {
  factory _$$_InputControllerCopyWith(
          _$_InputController value, $Res Function(_$_InputController) then) =
      __$$_InputControllerCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {TextEditingController controller,
      String? error,
      AuthInputType authInputType});
}

/// @nodoc
class __$$_InputControllerCopyWithImpl<$Res>
    extends _$InputControllerCopyWithImpl<$Res, _$_InputController>
    implements _$$_InputControllerCopyWith<$Res> {
  __$$_InputControllerCopyWithImpl(
      _$_InputController _value, $Res Function(_$_InputController) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? controller = null,
    Object? error = freezed,
    Object? authInputType = null,
  }) {
    return _then(_$_InputController(
      controller: null == controller
          ? _value.controller
          : controller // ignore: cast_nullable_to_non_nullable
              as TextEditingController,
      error: freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String?,
      authInputType: null == authInputType
          ? _value.authInputType
          : authInputType // ignore: cast_nullable_to_non_nullable
              as AuthInputType,
    ));
  }
}

/// @nodoc

class _$_InputController implements _InputController {
  const _$_InputController(
      {required this.controller,
      required this.error,
      required this.authInputType});

  @override
  final TextEditingController controller;
  @override
  final String? error;
  @override
  final AuthInputType authInputType;

  @override
  String toString() {
    return 'InputController(controller: $controller, error: $error, authInputType: $authInputType)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InputController &&
            (identical(other.controller, controller) ||
                other.controller == controller) &&
            (identical(other.error, error) || other.error == error) &&
            (identical(other.authInputType, authInputType) ||
                other.authInputType == authInputType));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, controller, error, authInputType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InputControllerCopyWith<_$_InputController> get copyWith =>
      __$$_InputControllerCopyWithImpl<_$_InputController>(this, _$identity);
}

abstract class _InputController implements InputController {
  const factory _InputController(
      {required final TextEditingController controller,
      required final String? error,
      required final AuthInputType authInputType}) = _$_InputController;

  @override
  TextEditingController get controller;
  @override
  String? get error;
  @override
  AuthInputType get authInputType;
  @override
  @JsonKey(ignore: true)
  _$$_InputControllerCopyWith<_$_InputController> get copyWith =>
      throw _privateConstructorUsedError;
}
