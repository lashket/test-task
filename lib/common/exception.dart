import 'package:freezed_annotation/freezed_annotation.dart';

part 'exception.freezed.dart';

@freezed
abstract class AppException implements Exception, _$AppException {
  factory AppException.authException(String message) = AuthException;
  factory AppException.unknownError(String message) = UnknownError;
  factory AppException.invalidCredentials() = InvalidCredentials;
  factory AppException.emailAlreadyTaken() = EmailAlreadyTaken;
  factory AppException.validationException(String message) = ValidationException;
}
